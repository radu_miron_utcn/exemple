package edu.utcn.str.c5.ex4;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class App4Thread extends Thread {
    private Lock lock;
    private int min;
    private int max;
    private int t;

    public App4Thread(Lock lock, int min, int max, int t) {
        this.lock = lock;
        this.min = min;
        this.max = max;
        this.t = t;
    }

    public void run() {
        while (true) {
            ActivityUtils.activity("A1");

            lock.lock();
            ActivityUtils.timedActivity(min, max, "A2");
            lock.unlock();

            ActivityUtils.activity("A3");
            ActivityUtils.timedTransition(t);
            ActivityUtils.activity("A4");
        }
    }
}
