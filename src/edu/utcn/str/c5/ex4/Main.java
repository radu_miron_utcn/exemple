package edu.utcn.str.c5.ex4;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        new App4Thread(lock, 4, 7, 3).start();
        new App4Thread(lock, 5, 7, 6).start();
        new App4Thread(lock, 3, 6, 5).start();
    }
}
