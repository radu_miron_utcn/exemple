package edu.utcn.str.c5.util;

/**
 * @author Radu Miron
 * @version 1
 */
public class ActivityUtils {

    public static void timedActivity(int min, int max, String activityName) {
        System.out.println(String.format("Start activity %s", activityName));

        int k = (int) Math.round(Math.random() * (max - min) + min);

        for (int i = 0; i < k * 100000; i++) {
            i++;
            i--;
        }
    }

    public static void activity(String activityName) {
        System.out.println(String.format("Start activity %s", activityName));
    }

    public static void timedTransition(int t) {
        try {
            Thread.sleep(t * 1000);
        } catch (InterruptedException ignored) {
        }
    }
}
