package edu.utcn.str.c5.ex3cdl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        CountDownLatch countDownLatch1 = new CountDownLatch(1);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

        new App3Thread1(countDownLatch1, countDownLatch2, cyclicBarrier, 7, 2, 3).start();
        new App3Thread2(countDownLatch1, cyclicBarrier, 5, 2, 3).start();
        new App3Thread2(countDownLatch2, cyclicBarrier, 5, 2, 3).start();
    }
}
