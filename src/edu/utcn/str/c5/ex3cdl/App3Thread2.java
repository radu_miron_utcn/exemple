package edu.utcn.str.c5.ex3cdl;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 * @author Radu Miron
 * @version 1
 */
public class App3Thread2 extends Thread {
    private CountDownLatch countDownLatch;
    private CyclicBarrier cyclicBarrier;
    private int t;
    private int min;
    private int max;

    public App3Thread2(CountDownLatch countDownLatch, CyclicBarrier cyclicBarrier, int t, int min, int max) {
        this.countDownLatch = countDownLatch;
        this.cyclicBarrier = cyclicBarrier;
        this.t = t;
        this.min = min;
        this.max = max;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");

        ActivityUtils.timedTransition(t);

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }

        ActivityUtils.timedActivity(min, max, "A2");
        ActivityUtils.activity("A3");

        try {
            cyclicBarrier.await();
        } catch (Exception ignored) {
        }
    }
}
