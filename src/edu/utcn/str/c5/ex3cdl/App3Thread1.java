package edu.utcn.str.c5.ex3cdl;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class App3Thread1 extends Thread {
    private CountDownLatch countDownLatch1;
    private CountDownLatch countDownLatch2;
    private CyclicBarrier cyclicBarrier;
    private int t;
    private int min;
    private int max;

    public App3Thread1(CountDownLatch countDownLatch1, CountDownLatch countDownLatch2, CyclicBarrier cyclicBarrier, int t, int min, int max) {
        this.countDownLatch1 = countDownLatch1;
        this.countDownLatch2 = countDownLatch2;
        this.cyclicBarrier = cyclicBarrier;
        this.t = t;
        this.min = min;
        this.max = max;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");
        ActivityUtils.timedTransition(t);
        ActivityUtils.timedActivity(min, max, "A2");

        countDownLatch1.countDown();
        countDownLatch2.countDown();

        ActivityUtils.activity("A3");

        try {
            cyclicBarrier.await();
        } catch (Exception ignored) {
        }
    }
}
