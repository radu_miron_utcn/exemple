package edu.utcn.str.c5.ex1;

import java.util.concurrent.CountDownLatch;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock1 = new Object(); // replace this with a ReentrantLock
        Object lock2 = new Object(); // replace this with a Semaphore
        CountDownLatch countDownLatch = new CountDownLatch(2); // replace this with a CyclicBarrier

        new App1Thread(lock1, lock2, countDownLatch, 2, 4, 4, 6, 4).start();
        new App1Thread(lock2, lock1, countDownLatch, 3, 5, 5, 7, 5).start();
    }
}
