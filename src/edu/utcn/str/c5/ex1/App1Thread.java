package edu.utcn.str.c5.ex1;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.CountDownLatch;

/**
 * @author Radu Miron
 * @version 1
 */
public class App1Thread extends Thread {
    private Object lock1;
    private Object lock2;
    private CountDownLatch countDownLatch;
    private int min1;
    private int max1;
    private int min2;
    private int max2;
    private int t;

    public App1Thread(Object lock1, Object lock2, CountDownLatch countDownLatch, int min1, int max1, int min2, int max2, int t) {
        this.lock1 = lock1;
        this.lock2 = lock2;
        this.countDownLatch = countDownLatch;
        this.min1 = min1;
        this.max1 = max1;
        this.min2 = min2;
        this.max2 = max2;
        this.t = t;
    }

    @Override
    public void run() {
        // replace the count down latch with CyclicBarrier
        // add everything in a while loop

        ActivityUtils.timedActivity(min1, max1, "A1");

        synchronized (lock1) { // lock1.lock(); SAU semaphore1.acquire();
            ActivityUtils.timedActivity(min2, max2, "A2");

            synchronized (lock2) { // lock2.lock(); SAU semaphore2.acquire();
                ActivityUtils.activity("A3");
            } // lock2.unlock(); SAU semaphore2.release();
        } // lock1.unlock(); SAU semaphore1.release();

        ActivityUtils.timedTransition(t);

        ActivityUtils.activity("A4");

        countDownLatch.countDown();

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
    }
}
