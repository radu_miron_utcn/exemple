package edu.utcn.str.c5.ex3;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class App3Thread1 extends Thread {
    private Semaphore semaphore1;
    private Semaphore semaphore2;
    private CyclicBarrier cyclicBarrier;
    private int t;
    private int min;
    private int max;

    public App3Thread1(Semaphore semaphore1, Semaphore semaphore2, CyclicBarrier cyclicBarrier, int t, int min, int max) {
        this.semaphore1 = semaphore1;
        this.semaphore2 = semaphore2;
        this.cyclicBarrier = cyclicBarrier;
        this.t = t;
        this.min = min;
        this.max = max;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");
        ActivityUtils.timedTransition(t);
        ActivityUtils.timedActivity(min, max, "A2");

        semaphore1.release();
        semaphore2.release();

        ActivityUtils.activity("A3");

        try {
            cyclicBarrier.await();
        } catch (Exception ignored) {
        }
    }
}
