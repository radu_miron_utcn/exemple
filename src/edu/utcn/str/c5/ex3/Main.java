package edu.utcn.str.c5.ex3;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore1 = new Semaphore(0);
        Semaphore semaphore2 = new Semaphore(0);
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

        new App3Thread1(semaphore1, semaphore2, cyclicBarrier, 7, 2, 3).start();
        new App3Thread2(semaphore1, cyclicBarrier, 5, 2, 3).start();
        new App3Thread2(semaphore2, cyclicBarrier, 5, 2, 3).start();
    }
}
