package edu.utcn.str.c5.ex2;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class App2Thread2 extends Thread {
    private Lock lock1;
    private Lock lock2;
    private CountDownLatch countDownLatch;
    private int min;
    private int max;
    private int t;

    public App2Thread2(Lock lock1, Lock lock2, CountDownLatch countDownLatch, int min, int max, int t) {
        this.lock1 = lock1;
        this.lock2 = lock2;
        this.countDownLatch = countDownLatch;
        this.min = min;
        this.max = max;
        this.t = t;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");

        lock1.lock();
        lock2.lock();

        ActivityUtils.timedActivity(min, max, "A2");

        ActivityUtils.timedTransition(t);

        lock1.unlock();
        lock2.unlock();

        ActivityUtils.activity("A3");

        countDownLatch.countDown();

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
    }
}
