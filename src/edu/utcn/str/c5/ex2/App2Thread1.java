package edu.utcn.str.c5.ex2;

import edu.utcn.str.c5.util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class App2Thread1 extends Thread {
    private Lock lock;
    private CountDownLatch countDownLatch;
    private int min;
    private int max;
    private int t;

    public App2Thread1(Lock lock, CountDownLatch countDownLatch, int min, int max, int t) {
        this.lock = lock;
        this.countDownLatch = countDownLatch;
        this.min = min;
        this.max = max;
        this.t = t;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");

        lock.lock();

        ActivityUtils.timedActivity(min, max, "A2");

        ActivityUtils.timedTransition(t);

        lock.unlock();

        ActivityUtils.activity("A3");

        countDownLatch.countDown();

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
    }
}
