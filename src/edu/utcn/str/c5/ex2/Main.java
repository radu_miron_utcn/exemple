package edu.utcn.str.c5.ex2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Lock lock1 = new ReentrantLock();
        Lock lock2 = new ReentrantLock();
        CountDownLatch countDownLatch = new CountDownLatch(3);

        new App2Thread1(lock1, countDownLatch, 2, 4, 4).start();
        new App2Thread1(lock2, countDownLatch, 2, 5, 5).start();
        new App2Thread2(lock1, lock2, countDownLatch, 3, 6, 3).start();
    }
}
