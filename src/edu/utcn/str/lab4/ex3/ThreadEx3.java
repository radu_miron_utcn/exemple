package edu.utcn.str.lab4.ex3;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadEx3 extends Thread {
    private Object syncObj;
    private int pMin;
    private int pMax;
    private int t;

    public ThreadEx3(Object syncObj, int pMin, int pMax, int t) {
        this.syncObj = syncObj;
        this.pMin = pMin;
        this.pMax = pMax;
        this.t = t;
    }

    @Override
    public void run() {
        while (true) {
            ActivityUtils.doActivity("1");

            synchronized (syncObj) {
                ActivityUtils.doTimedActivity("2", pMin, pMax);
            }

            ActivityUtils.doTimedTransition(t);

            ActivityUtils.doActivity("3");
        }
    }
}
