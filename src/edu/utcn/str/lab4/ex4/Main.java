package edu.utcn.str.lab4.ex4;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = new Object();

        new Notifier(o1, o2, 7, 2, 3).start();
        new Waiter(o1, 3, 5).start();
        new Waiter(o2, 4, 6).start();
    }
}
