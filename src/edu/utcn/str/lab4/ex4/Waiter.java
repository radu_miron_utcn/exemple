package edu.utcn.str.lab4.ex4;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class Waiter extends Thread {
    private Object syncObj;
    private int pMin;
    private int pMax;

    public Waiter(Object syncObj, int pMin, int pMax) {
        this.syncObj = syncObj;
        this.pMin = pMin;
        this.pMax = pMax;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");

        synchronized (syncObj) {
            try {
                syncObj.wait();
            } catch (InterruptedException e) {
            }
        }

        ActivityUtils.doTimedActivity("2", pMin, pMax);
        ActivityUtils.doActivity("3");
    }
}
