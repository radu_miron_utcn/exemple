package edu.utcn.str.lab4.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object syncObj1 = new Object();
        Object syncObj2 = new Object();

        new Thread1Ex1(syncObj1, 2, 4, 4).start();
        new Thread2Ex1(syncObj1, syncObj2, 3, 6, 3).start();
        new Thread1Ex1(syncObj2, 2, 5, 5).start();
    }
}
