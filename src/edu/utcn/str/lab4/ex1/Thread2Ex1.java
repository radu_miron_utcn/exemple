package edu.utcn.str.lab4.ex1;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class Thread2Ex1 extends Thread {
    private Object syncObj1;
    private Object syncObj2;
    private int pMin;
    private int pMax;
    private int t;

    public Thread2Ex1(Object syncObj1, Object syncObj2, int pMin, int pMax, int t) {
        this.syncObj1 = syncObj1;
        this.syncObj2 = syncObj2;
        this.pMin = pMin;
        this.pMax = pMax;
        this.t = t;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");

        synchronized (syncObj1) {
            synchronized (syncObj2) {
                ActivityUtils.doTimedActivity("2", pMin, pMax);
                ActivityUtils.doTimedTransition(t);
            }
        }

        ActivityUtils.doActivity("3");
    }
}
