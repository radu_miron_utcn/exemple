package edu.utcn.str.lab4.ex1;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class Thread1Ex1 extends Thread {
    private Object syncObj;
    private int pMin;
    private int pMax;
    private int t;

    public Thread1Ex1(Object syncObj, int pMin, int pMax, int t) {
        this.syncObj = syncObj;
        this.pMin = pMin;
        this.pMax = pMax;
        this.t = t;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");

        synchronized (syncObj) {
            ActivityUtils.doTimedActivity("2", pMin, pMax);
            ActivityUtils.doTimedTransition(t);
        }

        ActivityUtils.doActivity("3");
    }
}
