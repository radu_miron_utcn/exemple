package edu.utcn.str.c4.common;

/**
 * @author Radu Miron
 * @version 1
 */
public class ActivityUtils {

    private ActivityUtils() {
    }

    public static void doActivity(String activityName) {
        System.out.println(Thread.currentThread().getName() + " is performing activity " + activityName);
    }

    public static void doTimedActivity(String activityName, int min, int max) {
        System.out.println(Thread.currentThread().getName() + " is performing activity " + activityName);

        int k = (int) Math.round(Math.random() * (max - min) + min);
        for (int i = 0; i < k * 10000; i++) {
            i++;
            i--;
        }

        try {
            Thread.sleep(k * 1000);
        } catch (InterruptedException e) {
        }
    }

    public static void doTimedTransition(int t) {
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
        }
    }
}
