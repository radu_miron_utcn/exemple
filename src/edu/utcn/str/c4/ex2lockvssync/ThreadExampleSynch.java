package edu.utcn.str.c4.ex2lockvssync;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadExampleSynch extends Thread {
    private Object synchObj;

    public ThreadExampleSynch(Object synchObj) {
        this.synchObj = synchObj;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");

        synchronized (synchObj) {
            ActivityUtils.doActivity("2");
        }

        ActivityUtils.doActivity("3");
    }
}
