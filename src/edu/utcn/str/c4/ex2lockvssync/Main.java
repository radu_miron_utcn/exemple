package edu.utcn.str.c4.ex2lockvssync;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        new ThreadExampleLock(lock).start();
        new ThreadExampleLock(lock).start();
    }
}
