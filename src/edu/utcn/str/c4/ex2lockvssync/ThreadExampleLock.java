package edu.utcn.str.c4.ex2lockvssync;

import edu.utcn.str.c4.common.ActivityUtils;

import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadExampleLock extends Thread {
    private Lock lock;

    public ThreadExampleLock(Lock lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");

        lock.lock();
        try {
            ActivityUtils.doActivity("2");
        } finally {
            lock.unlock();
        }

        ActivityUtils.doActivity("3");
    }
}
