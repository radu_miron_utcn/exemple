package edu.utcn.str.c4.ex6waitnotifycdl;

import java.util.concurrent.CountDownLatch;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        CountDownLatch cdl = new CountDownLatch(1);
        new Notifier(cdl).start();
        new Waiter(cdl).start();
    }
}
