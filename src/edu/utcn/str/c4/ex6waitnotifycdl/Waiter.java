package edu.utcn.str.c4.ex6waitnotifycdl;

import java.util.concurrent.CountDownLatch;

/**
 * @author Radu Miron
 * @version 1
 */
public class Waiter extends Thread {
    private CountDownLatch cdl;

    public Waiter(CountDownLatch cdl) {
        this.cdl = cdl;
    }

    @Override
    public void run() {
        try {
            cdl.await(); // this line of code is equivalent with wait()
            System.out.println("I've been unlocked");
        } catch (InterruptedException e) {
        }
    }
}
