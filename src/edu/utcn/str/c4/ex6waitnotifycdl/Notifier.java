package edu.utcn.str.c4.ex6waitnotifycdl;

import java.util.concurrent.CountDownLatch;

/**
 * @author Radu Miron
 * @version 1
 */
public class Notifier extends Thread {
    private CountDownLatch cdl;

    public Notifier(CountDownLatch cdl) {
        this.cdl = cdl;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        cdl.countDown(); // this line of code is equivalent with notify()
        System.out.println("Notify has been issued");

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
    }
}
