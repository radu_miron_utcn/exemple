package edu.utcn.str.c4.ex4cyclicbarrier;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Map<String, Integer> resultsByThreadName = new ConcurrentHashMap<>();

        CyclicBarrier cb = new CyclicBarrier(3, () -> {
            System.out.println(Thread.currentThread().getName() + " final result: " +
                    resultsByThreadName.values().stream()
                            .reduce((a, b) -> a + b)
                            .orElse(0)
            );
        });

        new ThreadCyclicBarrierExample(cb, resultsByThreadName).start();
        new ThreadCyclicBarrierExample(cb, resultsByThreadName).start();
        new ThreadCyclicBarrierExample(cb, resultsByThreadName).start();
    }
}
