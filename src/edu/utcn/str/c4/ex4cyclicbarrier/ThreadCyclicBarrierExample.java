package edu.utcn.str.c4.ex4cyclicbarrier;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadCyclicBarrierExample extends Thread {
    private CyclicBarrier cb;
    private Map<String, Integer> resultsByThreadName;

    public ThreadCyclicBarrierExample(CyclicBarrier cb, Map<String, Integer> resultsByThreadName) {
        this.cb = cb;
        this.resultsByThreadName = resultsByThreadName;
    }

    @Override
    public void run() {
        while (true) {
            int partialResult = new Random().nextInt();
            resultsByThreadName.put(this.getName(), partialResult);
            System.out.println(this.getName() + " result: " + partialResult);

            try {
                Thread.sleep(new Random().nextInt(5) * 1000);
                cb.await();
            } catch (Exception e) {
            }
        }
    }
}
