package edu.utcn.str.c4.ex1volatile;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Singleton {
    private volatile static Singleton instance;
    private int n;

    public Singleton(int n) {
        this.n = n;
    }

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton(new Random().nextInt(100));
        }

        return instance;
    }
}
