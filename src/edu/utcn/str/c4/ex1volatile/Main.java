package edu.utcn.str.c4.ex1volatile;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {

    public static void main(String[] args) {
        Set<String> references = new CopyOnWriteArraySet<>();

        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                references.add(Singleton.getInstance().toString());
            }).start();
        }

        System.out.println(references.size());
    }
}

