package edu.utcn.str.c4.ex5waitnotifysemaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Notifier extends Thread {
    private Semaphore semaphore;

    public Notifier(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        semaphore.release(); // this line of code is equivalent with notify()
        System.out.println("Notify has been issued");

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
    }
}
