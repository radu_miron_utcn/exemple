package edu.utcn.str.c4.ex5waitnotifysemaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(0);
        new Notifier(semaphore).start();
        new Waiter(semaphore).start();
    }
}
