package edu.utcn.str.c4.ex5waitnotifysemaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Waiter extends Thread {
    private Semaphore semaphore;

    public Waiter(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire(); // this line of code is equivalent with wait()
            System.out.println("I've been unlocked");
        } catch (InterruptedException e) {
        }
    }
}
