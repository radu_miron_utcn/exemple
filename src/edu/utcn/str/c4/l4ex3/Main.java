package edu.utcn.str.c4.l4ex3;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object syncObj = new Object();
        new ThreadEx3(syncObj, 3, 6, 5).start();
        new ThreadEx3(syncObj, 5, 7, 6).start();
        new ThreadEx3(syncObj, 4, 7, 3).start();
    }
}
