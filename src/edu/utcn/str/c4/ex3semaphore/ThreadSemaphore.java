package edu.utcn.str.c4.ex3semaphore;

import edu.utcn.str.c4.common.ActivityUtils;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadSemaphore extends Thread {
    private Semaphore semaphore;
    private int t;
    private int pMin;
    private int pMax;
    private int acquiredPermits;
    private int releasedPermits;

    public ThreadSemaphore(Semaphore semaphore, int t, int pMin, int pMax, int acquiredPermits, int releasedPermits) {
        this.semaphore = semaphore;
        this.t = t;
        this.pMin = pMin;
        this.pMax = pMax;
        this.acquiredPermits = acquiredPermits;
        this.releasedPermits = releasedPermits;
    }

    @Override
    public void run() {
        while (true) {
            ActivityUtils.doActivity("1");
            ActivityUtils.doTimedTransition(t);
            ActivityUtils.doActivity("2");

            try {
                semaphore.acquire(acquiredPermits);
                ActivityUtils.doTimedActivity("3", pMin, pMax);
            } catch (InterruptedException e) {
            } finally {
                semaphore.release(releasedPermits);
            }

            ActivityUtils.doActivity("4");
        }
    }
}
