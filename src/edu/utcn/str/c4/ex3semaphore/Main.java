package edu.utcn.str.c4.ex3semaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1);
        new ThreadSemaphore(semaphore, 2, 2, 5, 2, 1).start();
        new ThreadSemaphore(semaphore, 4, 3, 6, 1, 2).start();
    }
}
