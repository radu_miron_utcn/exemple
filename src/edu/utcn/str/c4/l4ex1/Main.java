package edu.utcn.str.c4.l4ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object syncObj1 = new Object();
        Object syncObj2 = new Object();

        new Thread1(syncObj1, 2, 4, 4).start();
        new Thread2(syncObj1, syncObj2, 3, 6, 3).start();
        new Thread1(syncObj2,  2, 5, 5).start();
    }
}
