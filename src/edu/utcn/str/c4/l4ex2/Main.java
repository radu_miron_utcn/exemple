package edu.utcn.str.c4.l4ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object syncObj1 = new Object();
        Object syncObj2 = new Object();

        new ThreadEx2(syncObj1, syncObj2, 2, 4, 4, 6, 4).start();
        new ThreadEx2(syncObj2, syncObj1, 3, 5, 5, 7, 5).start();
    }
}
