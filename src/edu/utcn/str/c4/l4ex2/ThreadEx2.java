package edu.utcn.str.c4.l4ex2;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadEx2 extends Thread {
    private Object syncObj1;
    private Object syncObj2;
    private int p1Min;
    private int p1Max;
    private int p2Min;
    private int p2Max;
    private int t;

    public ThreadEx2(Object syncObj1, Object syncObj2, int p1Min, int p1Max, int p2Min, int p2Max, int t) {
        this.syncObj1 = syncObj1;
        this.syncObj2 = syncObj2;
        this.p1Min = p1Min;
        this.p1Max = p1Max;
        this.p2Min = p2Min;
        this.p2Max = p2Max;
        this.t = t;
    }

    @Override
    public void run() {
        ActivityUtils.doTimedActivity("1", p1Min, p1Max);

        synchronized (syncObj1) {
            ActivityUtils.doTimedActivity("2", p2Min, p2Max);

            synchronized (syncObj2) {
                ActivityUtils.doActivity("3");
            }
        }

        ActivityUtils.doTimedTransition(t);
        ActivityUtils.doActivity("4");
    }
}
