package edu.utcn.str.c4.l4ex4;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadEx41 extends Thread {
    private Object syncObj1;
    private Object syncObj2;
    private int t;
    private int pMin;
    private int pMax;

    public ThreadEx41(Object syncObj1, Object syncObj2, int t, int pMin, int pMax) {
        this.syncObj1 = syncObj1;
        this.syncObj2 = syncObj2;
        this.t = t;
        this.pMin = pMin;
        this.pMax = pMax;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");
        ActivityUtils.doTimedTransition(t);
        ActivityUtils.doTimedActivity("2", pMin, pMax);

        synchronized (syncObj1) {
            syncObj1.notify();
        }

        synchronized (syncObj2) {
            syncObj2.notify();
        }

        ActivityUtils.doActivity("3");
    }
}
