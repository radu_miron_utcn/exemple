package edu.utcn.str.c4.l4ex4;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object syncObj1 = new Object();
        Object syncObj2 = new Object();

        new ThreadEx41(syncObj1, syncObj2, 7, 2, 3).start();
        new ThreadEx42(syncObj1, 3, 5).start();
        new ThreadEx42(syncObj2, 4, 6).start();
    }
}
