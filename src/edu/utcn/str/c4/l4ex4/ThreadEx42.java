package edu.utcn.str.c4.l4ex4;

import edu.utcn.str.c4.common.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadEx42 extends Thread {
    private Object syncObj;
    private int pMin;
    private int pMax;

    public ThreadEx42(Object syncObj, int pMin, int pMax) {
        this.syncObj = syncObj;
        this.pMin = pMin;
        this.pMax = pMax;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("1");

        synchronized (syncObj) {
            try {
                syncObj.wait();
            } catch (InterruptedException e) {
            }
        }

        ActivityUtils.doTimedActivity("2", pMin, pMax);

        ActivityUtils.doActivity("3");
    }
}
