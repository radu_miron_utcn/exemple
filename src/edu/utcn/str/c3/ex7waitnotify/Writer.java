package edu.utcn.str.c3.ex7waitnotify;

import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Writer extends Thread {
    List<String> messages;

    public Writer(List<String> messages) {
        this.messages = messages;
    }

    @Override
    public void run() {
        synchronized (messages) {
            try {
                messages.wait();
            } catch (InterruptedException e) {
            }

            System.out.println();
            System.out.println("The messages:");
            messages.forEach(m -> System.out.println(m));
        }
    }
}
