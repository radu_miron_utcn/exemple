package edu.utcn.str.c3.ex7waitnotify;

import java.util.List;
import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Reader extends Thread {
    List<String> messages;

    public Reader(List<String> messages) {
        this.messages = messages;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        String message = "";
        System.out.println("Please write your messages (exit with finish):");

        while (!message.toLowerCase().equals("finish")) {
            message = scanner.nextLine();
            messages.add(message);
        }

        synchronized (messages) {
            messages.notify();
        }
    }
}
