package edu.utcn.str.c3.ex7waitnotify;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        List<String> messages = new ArrayList<>();
        new Reader(messages).start();
        new Writer(messages).start();
    }
}
