package edu.utcn.str.c3.ex4synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    @Override
    public void run() {
        doActivity(1);

        synchronized (MyThread.class) {
            doActivity(2);
        }

        doActivity(3);
    }

    private void doActivity(int activityNum) {
        System.out.println(Thread.currentThread().getName() + " - Start activity " + activityNum);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }
}
