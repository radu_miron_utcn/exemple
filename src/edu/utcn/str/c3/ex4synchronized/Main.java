package edu.utcn.str.c3.ex4synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new MyThread().start();
        new MyThread().start();
    }
}
