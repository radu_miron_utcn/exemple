package edu.utcn.str.c3.ex8waitnotify;

import java.util.List;
import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Reader extends Thread {
    Object object;

    public Reader(Object object) {
        this.object = object;
    }

    @Override
    public void run() {

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }

        synchronized (object) {
            object.notify();
            System.out.println("Notify has been issued");

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }
}
