package edu.utcn.str.c3.ex8waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class Writer extends Thread {
    Object object;

    public Writer(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object) {
            try {
                object.wait();
                System.out.println("I've been unlocked");
            } catch (InterruptedException e) {
            }
        }
    }
}
