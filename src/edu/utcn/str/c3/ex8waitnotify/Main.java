package edu.utcn.str.c3.ex8waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object obj = new Object();
        new Reader(obj).start();
        new Writer(obj).start();
    }
}
