package edu.utcn.str.c3.ex5deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class DeadlockedThread extends Thread {

    private Object lock1;
    private Object lock2;

    public DeadlockedThread(Object lock1, Object lock2) {
        this.lock1 = lock1;
        this.lock2 = lock2;
    }

    @Override
    public void run() {
        doActivity(1);

        synchronized (lock1) {
            doActivity(2);

            synchronized (lock2) {
                doActivity(3);
            }
        }

        doActivity(4);
    }

    private void doActivity(int activityNum) {
        System.out.println(Thread.currentThread().getName() + " - Start activity " + activityNum);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }
}
