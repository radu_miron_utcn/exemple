package edu.utcn.str.c3.ex5deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object p9 = new Object();
        Object p10 = new Object();

        new DeadlockedThread(p9, p10).start();
        new DeadlockedThread(p10, p9).start();
    }
}
