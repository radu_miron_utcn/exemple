package edu.utcn.str.c3.ex6fixfordeadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object p9 = new Object();

        new DeadlockedThread(p9).start();
        new DeadlockedThread(p9).start();
    }
}
