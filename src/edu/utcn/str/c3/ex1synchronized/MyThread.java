package edu.utcn.str.c3.ex1synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private final Object usedForItsMonitor;

    public MyThread(Object usedForItsMonitor) {
        this.usedForItsMonitor = usedForItsMonitor;

        // if we use different objects in the synchronized block, the synchronization will not work
//        this.usedForItsMonitor = new Object();
    }

    @Override
    public void run() {
        doActivity(1);

        synchronized (usedForItsMonitor) {
            doActivity(2);
        }

        doActivity(3);
    }

    private void doActivity(int activityNum) {
        System.out.println(Thread.currentThread().getName() + " - Start activity " + activityNum);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }
}
