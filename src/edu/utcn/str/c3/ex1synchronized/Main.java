package edu.utcn.str.c3.ex1synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object usedForItsMonitor = new Object();

        new MyThread(usedForItsMonitor).start();
        new MyThread(usedForItsMonitor).start();
    }
}
