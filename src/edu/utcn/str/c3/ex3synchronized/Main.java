package edu.utcn.str.c3.ex3synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        MyCounter myCounter = new MyCounter();

        IncrementerThread incrementerThread1 = new IncrementerThread(myCounter);
        IncrementerThread incrementerThread2 = new IncrementerThread(myCounter);
        IncrementerThread incrementerThread3 = new IncrementerThread(myCounter);

        incrementerThread1.start();
        incrementerThread2.start();
        incrementerThread3.start();

        incrementerThread1.join();
        incrementerThread2.join();
        incrementerThread3.join();

        System.out.println(Thread.currentThread().getName()+": Counter value: " + myCounter.getCounter());
    }
}
