package edu.utcn.str.c3.ex3synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyCounter {
    private volatile int counter = 0;

    public synchronized void increment() {
        counter += 1;
    }

    public int getCounter() {
        return this.counter;
    }
}
