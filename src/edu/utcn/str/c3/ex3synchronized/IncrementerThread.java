package edu.utcn.str.c3.ex3synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class IncrementerThread extends Thread {
    private MyCounter myCounter;

    public IncrementerThread(MyCounter myCounter) {
        System.out.println(Thread.currentThread().getName() + " created");
        this.myCounter = myCounter;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " started");

        for (int i = 0; i < 100000; i++) {
            myCounter.increment();
        }
    }
}
