package edu.utcn.str.c1.ex2inheritance;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class Main {
    public static void main(String[] args) {
        DaciaLogan daciaLogan = new DaciaLogan(200, "white");
        VwTRoc vwTRoc = new VwTRoc(240, "black");

        daciaLogan.go();
        vwTRoc.go();

        daciaLogan.testTypeCast();

        Car newDaciaLoganReference = daciaLogan;

//        not valid
//        newDaciaLoganReference.testTypeCast();

        DaciaLogan daciaLogan1 = (DaciaLogan) newDaciaLoganReference;
        daciaLogan1.testTypeCast();

//        not valid
//        VwTRoc vwTRoc1 = (VwTRoc) newDaciaLoganReference;
//        vwTRoc1.go();

        DaciaLogan daciaLoganLast = new DaciaLogan(180, "green");
        Car daciaLoganAsCar = daciaLoganLast;
//        not valid
//        DaciaLogan16Gpl daciaLogan16Gpl = (DaciaLogan16Gpl) daciaLoganLast;

        System.out.println("Is obj DaciaLogan?" + (daciaLoganLast instanceof DaciaLogan));
        System.out.println("Is obj Car?" + (daciaLoganLast instanceof Car));
        System.out.println("Is obj DaciaLogan16Gpl?" + (daciaLoganLast instanceof DaciaLogan16Gpl));
    }
}
