package edu.utcn.str.c1.ex2inheritance;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class DaciaLogan16Gpl extends DaciaLogan {
    public DaciaLogan16Gpl(int topSpeed, String color) {
        super(topSpeed, color);
    }

    public void met(){
        System.out.println();
    }
}
