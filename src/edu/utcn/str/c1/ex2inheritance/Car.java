package edu.utcn.str.c1.ex2inheritance;

/**
 * @author Radu Miron
 * @version 1
 */
public abstract class Car {
    private int topSpeed;
    private String color;

    public Car(int topSpeed, String color) {
        this.topSpeed = topSpeed;
        this.color = color;
    }

    public void go() {
        System.out.println(String.format(" goes with the top speed of %d km/h", topSpeed));
    }
}
