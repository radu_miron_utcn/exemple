package edu.utcn.str.c1.ex3polymorphism;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose your car: 1.Dacia, 2.VW");
        int choice = scanner.nextInt();

        Car car;

        switch (choice) {
            case 1:
                car = new DaciaLogan(200, "red");
                break;
            case 2:
                car = new VwTRoc(240, "blue");
                break;
            default:
                throw new IllegalArgumentException("Invalid choice");
        }

        car.go();
    }
}
