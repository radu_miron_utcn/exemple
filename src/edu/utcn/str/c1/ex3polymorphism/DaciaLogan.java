package edu.utcn.str.c1.ex3polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class DaciaLogan extends Car {
    public DaciaLogan(int topSpeed, String color) {
        super(topSpeed, color);
    }

    @Override
    public void go() {
        System.out.print("Dacia Logan");
        super.go();
    }

    public void testTypeCast() {
        System.out.println("test type cast");
    }
}