package edu.utcn.str.c1.ex3polymorphism;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class Main2 {
    public static void main(String[] args) {
        System.out.println("Choose your car: 1. Dacia, 2. VW");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                DaciaLogan daciaLogan = new DaciaLogan(200, "yellow");
                daciaLogan.go();
                break;
            case 2:
                VwTRoc vwTRoc = new VwTRoc(240, "white");
                vwTRoc.go();
                break;
            default:
                throw new IllegalArgumentException("Invalid choice!");
        }
    }
}
