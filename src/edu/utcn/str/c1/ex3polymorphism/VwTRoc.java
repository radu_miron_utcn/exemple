package edu.utcn.str.c1.ex3polymorphism;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class VwTRoc extends Car {
    public VwTRoc(int topSpeed, String color) {
        super(topSpeed, color);
    }

    @Override
    public void go() {
        System.out.print("VW T-Roc");
        super.go();
    }
}
