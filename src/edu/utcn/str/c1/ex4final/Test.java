package edu.utcn.str.c1.ex4final;

import java.io.File;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class Test {
    public static void main(String[] args) {
        final int var = 5;

//        cannot be modified
//        var++;
    }
}


final class Clazz {
}

// cannot be extended
//class ClazzD extends Clazz {
//}

class A {
    public final void met(){
        String s = File.separator;
        int maxInt = Integer.MAX_VALUE;
    }
}

class B extends A{
//    cannot be overridden
//    public void met(){
//    }
}