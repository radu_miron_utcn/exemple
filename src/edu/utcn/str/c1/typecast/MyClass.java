package edu.utcn.str.c1.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyClass implements MyInterface {
    @Override
    public void test() {
        System.out.println("this is the method declared in the interface");
    }

    public void testMyClass() {
        System.out.println("this is the method declared in the MyClass");
    }
}
