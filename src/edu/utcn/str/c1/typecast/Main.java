package edu.utcn.str.c1.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MyEvolvedClass myEvolvedObject = new MyEvolvedClass();
        myEvolvedObject.testMyEvolvedClass();
        myEvolvedObject.testMyClass();
        myEvolvedObject.test();

        MyClass myObject = myEvolvedObject;
        myObject.test();
        myObject.testMyClass();
//        myObject.testMyEvolvedClass();

        MyInterface myDumbObject = myEvolvedObject;
        myDumbObject.test();
//        myDumbObject.testMyClass();
//        myDumbObject.testMyEvolvedClass();
    }
}
