package edu.utcn.str.c1.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyEvolvedClass extends MyClass {

    public void testMyEvolvedClass() {
        System.out.println("this is the method declared in the MyEvolvedClass");
    }
}
