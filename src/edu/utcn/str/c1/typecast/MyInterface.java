package edu.utcn.str.c1.typecast;

/**
 * @author Radu Miron
 * @version 1
 */
public interface MyInterface {
    void test();
}
