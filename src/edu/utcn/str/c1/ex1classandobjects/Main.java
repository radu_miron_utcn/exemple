package edu.utcn.str.c1.ex1classandobjects;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        // initial tests
        Bicycle b1 = new Bicycle("red", "Pegas", 20);
        b1.park();

        Bicycle b2 = new Bicycle("Blue", "BMX", 24);
        b2.park();

        Bicycle b3 = new Bicycle("Blue", "BMX", 24);
        b3.park();

        b1.collide(b2);

        // verify wheel size
        Scanner scanner = new Scanner(System.in);

        System.out.println("brand:");
        String brand = scanner.nextLine();

        System.out.println("color:");
        String color = scanner.nextLine();

        System.out.println("wheel size:");
        int wheelSize = scanner.nextInt();

        if(wheelSize < 0 || wheelSize > Bicycle.MAX_WHEEL_SIZE) {
            System.err.println("The wheel size is not correct. Max wheel size is: " + Bicycle.getMaxWheelSize());
        } else {
            Bicycle b4 = new Bicycle(color, brand, wheelSize);
            b4.park();
        }
    }
}
