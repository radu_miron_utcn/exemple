package edu.utcn.str.c1.ex1classandobjects;

/**
 * @author Radu Miron
 * @version 1
 */
public class Bicycle {
    // constants
    public static final int MAX_WHEEL_SIZE = 36;

    // attributes
    private String color;
    private String brand;
    private int wheelSize;

    //constructors
    public Bicycle(String color, String brand, int wheelSize) {
        this.color = color;
        this.brand = brand;
        this.wheelSize = wheelSize;
    }

    // methods
    public void park() {
        System.out.println("The " + this.toString() + " parked");
    }

    public void collide(Bicycle b) {
        System.out.println("The " + this.toString() + " crashes into " + b.toString());
    }

    @Override
    public String toString() {
        return "Bicycle{" +
                "color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                ", wheelSize=" + wheelSize +
                '}';
    }

    public static int getMaxWheelSize() {
        return MAX_WHEEL_SIZE;
    }
}
