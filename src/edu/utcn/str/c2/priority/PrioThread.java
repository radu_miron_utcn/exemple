package edu.utcn.str.c2.priority;

/**
 * @author Radu Miron
 * @version 1
 */
public class PrioThread extends Thread {
    int id;
    Window win;
    long processorLoad;

    PrioThread(int id, int priority, Window win, long procLoad) {
        this.id = id;
        this.win = win;
        this.processorLoad = procLoad;
        this.setPriority(priority);
    }

    public void run() {
        int c = 0;
        while (c < 1000) {
            for (int j = 0; j < this.processorLoad; j++) {
                j++;
                j--;
            }
            c++;
            this.win.setProgressValue(id, c);
        }
    }
}