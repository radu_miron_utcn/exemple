package edu.utcn.str.c2.priority;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    private static final int noOfThreads = 10;
    private static final long processorLoad = 10000000L;

    public static void main(String args[]) {
        Window win = new Window(noOfThreads);
        for (int i = 0; i < noOfThreads; i++) {
            new PrioThread(i, i + 1, win, processorLoad).start();
        }
    }
}