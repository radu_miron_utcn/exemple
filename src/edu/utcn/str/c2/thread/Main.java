package edu.utcn.str.c2.thread;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName() + " will start the threads now");

        Thread t1 = new ThreadWithClassThread("Ioan");
        Thread t2 = new ThreadWithClassThread("Maria");
        Thread t3 = new ThreadWithClassThread("Gheorghe");

        t1.start();
        t2.start();
        t3.start();
    }
}
