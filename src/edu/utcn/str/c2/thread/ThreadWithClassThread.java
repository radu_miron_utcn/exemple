package edu.utcn.str.c2.thread;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadWithClassThread extends Thread {
    public ThreadWithClassThread(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(this.getName() + ": message number " + i);

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // ignore
            }
        }
    }
}
