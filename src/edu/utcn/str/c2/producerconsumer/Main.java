package edu.utcn.str.c2.producerconsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayBlockingQueue<String> filesToProcess = new ArrayBlockingQueue<>(1000, true);

        Producer producer = new Producer(filesToProcess);
        producer.start();

        System.out.println("Number of consumers:");
        int consumersNumber = scanner.nextInt();

        List<Consumer> consumers = new ArrayList<>();

        for (int i = 0; i < consumersNumber; i++) {
            Consumer consumer = new Consumer("" + i, filesToProcess);
            consumers.add(consumer);
            consumer.start();
        }

        boolean stop = false;
        while (!stop) {
            System.out.println("Write stop if you want to stop the app:");
            String msg = scanner.nextLine();
            if (msg.toLowerCase().equals("stop")) {
                stop = true;
            }
        }

        producer.setStop(true);
        consumers.forEach(c -> {
            c.setStop(true);
            c.interrupt();
        });
    }
}
