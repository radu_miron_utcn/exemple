package edu.utcn.str.c2.producerconsumer;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Producer extends Thread {
    public static final String PRODUCER_DIR = "/tmp/producer_consumer/producer";
    private ArrayBlockingQueue<String> queue;
    private boolean stop;

    public Producer(ArrayBlockingQueue<String> queue) {
        this.queue = queue;
        FileUtils.makeDirIfNotExists(PRODUCER_DIR);
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            FileUtils.getNewFiles(PRODUCER_DIR)
                    .forEach(nfn -> {
                        try {
                            queue.put(nfn);
                        } catch (InterruptedException ignored) {
                        }
                    });
        }
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
