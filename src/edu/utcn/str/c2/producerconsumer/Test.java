package edu.utcn.str.c2.producerconsumer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Test {
    private static final String PRODUCER_DIR = "/tmp/producer_consumer/producer";
    private static final String CONSUMER_DIR = "/tmp/producer_consumer/consumer-1";

    public static void main(String[] args) throws IOException {
        FileUtils.makeDirIfNotExists(PRODUCER_DIR);

        System.out.println("initial get new files call");
        FileUtils.getNewFiles(PRODUCER_DIR).stream()
                .forEach(nf -> System.out.println(nf));

        FileWriter fw = new FileWriter(new File(new File(PRODUCER_DIR), "t6.txt"));
        fw.close();

        System.out.println();
        System.out.println("second get new files call");
        FileUtils.getNewFiles(PRODUCER_DIR).stream()
                .forEach(nf -> System.out.println(nf));

        System.out.println();
        System.out.println("extracted names from t1.txt");
        List<String> names = FileUtils.extractNamesFromFile(new File("/tmp/producer_consumer/producer/t1.txt"));
        names.stream()
                .forEach(e -> System.out.println(e));

        System.out.println();
        System.out.println("writing names to file");
        FileUtils.writeNamesToFile("t1.txt", CONSUMER_DIR, names);
    }
}
