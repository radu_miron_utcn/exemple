package edu.utcn.str.c2.producerconsumer;

import java.io.File;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Consumer extends Thread {
    public static final String CONSUMER_DIR_TEMPLATE = "/tmp/producer_consumer/consumer-";
    private ArrayBlockingQueue<String> queue;
    private String consumerDir;
    private boolean stop;

    public Consumer(String name, ArrayBlockingQueue<String> queue) {
        this.queue = queue;
        consumerDir = CONSUMER_DIR_TEMPLATE + name;
        this.setName(name);
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                String newFile = queue.take();
                File file = new File(newFile);
                List<String> names = FileUtils.extractNamesFromFile(file);
                FileUtils.writeNamesToFile(file.getName(), consumerDir, names);
                file.delete();
            } catch (Exception e) {
                System.out.println("Consumer " + this.getName() + " was interrupted");
            }
        }
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
