package edu.utcn.str.c2.producerconsumer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileUtils {
    private static Set<String> oldFiles = new HashSet<>();

    public static Set<String> getNewFiles(String dirPath) {
        File dir = new File(dirPath);

        if (!dir.exists()) {
            throw new IllegalArgumentException("The path does not exist!");
        }

        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("The path is not a directory");
        }

        Set<String> newFiles = Arrays.stream(Objects.requireNonNull(dir.listFiles()))
                .filter(f -> f.isFile() && f.getName().endsWith(".txt") && !oldFiles.contains(f.getAbsolutePath()))
                .map(File::getAbsolutePath)
                .collect(Collectors.toSet());
        oldFiles.addAll(newFiles);

        return newFiles;
    }

    public static void makeDirIfNotExists(String dirPath) {
        File dir = new File(dirPath);

        if (!dir.exists() || dir.isFile()) {
            dir.mkdirs();
        }
    }

    public static List<String> extractNamesFromFile(File file) throws IOException {
        return Files.lines(file.toPath())
                .filter(l -> l.startsWith("Name: "))
                .collect(Collectors.toList());
    }

    public static void writeNamesToFile(String fileName, String consumerDirPath, List<String> names) throws IOException {
        makeDirIfNotExists(consumerDirPath);
        File file = new File(new File(consumerDirPath), fileName);
        if (!file.exists()) {
            try (FileWriter fw = new FileWriter(file)) {
                names.forEach(name -> {
                    try {
                        fw.write(name + "\n");
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                fw.flush();
            }
        }
    }
}
