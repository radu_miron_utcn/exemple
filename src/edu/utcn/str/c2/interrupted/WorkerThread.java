package edu.utcn.str.c2.interrupted;

/**
 * @author Radu Miron
 * @version 1
 */
public class WorkerThread extends Thread {
    private boolean stop;

    @Override
    public void run() {
        this.setName(this.getClass().getSimpleName());

        while (!stop) {
            getOfficialExchangeRates();
            calculateMyExchangeRates();
            try {
                Thread.sleep(24 * 60 * 60 * 1000);
            } catch (InterruptedException e) {
                System.out.println(this.getName() + ": I've been interrupted");
            }
        }
    }

    private void calculateMyExchangeRates() {
        System.out.println("calculated my exchange rates");
    }

    private void getOfficialExchangeRates() {
        System.out.println("retrieved the exchange rates");
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
