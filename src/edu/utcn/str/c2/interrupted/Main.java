package edu.utcn.str.c2.interrupted;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        List<WorkerThread> workerThreads = new ArrayList<>();

        WorkerThread workerThread = new WorkerThread();
        workerThread.start();
        workerThreads.add(workerThread);

        Thread.sleep(5000);

        ShutDowner shutDowner = new ShutDowner(workerThreads);
        shutDowner.start();
    }
}
