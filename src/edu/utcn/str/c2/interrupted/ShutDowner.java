package edu.utcn.str.c2.interrupted;

import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class ShutDowner extends Thread {
    private List<WorkerThread> threadsToShutDown;

    public ShutDowner(List<WorkerThread> threadsToShutDown) {
        this.threadsToShutDown = threadsToShutDown;
        this.setName(this.getClass().getSimpleName());
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - start the shutdown procedure");

        for (WorkerThread t : threadsToShutDown) {
            System.out.println("Shutting down " + t.getName());
            t.setStop(true);
            t.interrupt();
        }
    }
}
