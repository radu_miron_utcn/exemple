package edu.utcn.str.c2.game;

import java.util.List;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        List<Enemy> enemies = new GUI().getEnemies();

        enemies.forEach(e -> {
            int speed = new Random().nextInt(5) + 5;
            new Engine(e, speed).start();
        });
    }
}
