package edu.utcn.str.c2.game;

/**
 * @author Radu Miron
 * @version 1
 */
public class Engine extends Thread {
    private Enemy enemy;
    private int speed;

    public Engine(Enemy enemy, int speed) {
        this.enemy = enemy;
        this.speed = speed;
    }

    @Override
    public void run() {
        while (true) {
            enemy.setBounds(enemy.getX(), enemy.getY() + speed, 20, 20);
            enemy.repaint();

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
