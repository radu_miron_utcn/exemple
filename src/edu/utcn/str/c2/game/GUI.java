package edu.utcn.str.c2.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class GUI extends JFrame implements KeyListener {
    private static final int SIZE = 700;
    private Character character = new Character();
    private java.util.List<Enemy> enemies;

    public GUI() throws HeadlessException {
        setTitle("My Game");
        setSize(SIZE, SIZE);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        addKeyListener(this);

        initEnemies(5);

        character.setBounds(350, 640, 20, 20);
        add(character);
        setVisible(true);
    }

    private void initEnemies(int enemiesNumber) {
        enemies = new ArrayList<>();
        double sections = enemiesNumber + 1;
        int distance = (int) Math.round(SIZE / sections);

        for (int i = 0; i < enemiesNumber; i++) {
            Enemy enemy = new Enemy();
            enemy.setBounds(distance * (i + 1), 0, 20, 20);
            enemies.add(enemy);
            add(enemy);
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        if (("" + keyEvent.getKeyChar()).equalsIgnoreCase("a")
                && character.getX() > 10) {
            character.setLocation(character.getX() - 10, character.getY());
            character.repaint();
        } else if (("" + keyEvent.getKeyChar()).equalsIgnoreCase("d")
                && character.getX() < 670) {
            character.setLocation(character.getX() + 10, character.getY());
            character.repaint();
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }
}
