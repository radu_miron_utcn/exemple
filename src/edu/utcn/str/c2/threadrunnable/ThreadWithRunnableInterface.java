package edu.utcn.str.c2.threadrunnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadWithRunnableInterface implements Runnable {
    private String name;

    public ThreadWithRunnableInterface(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(this.getName() + ": message number " + i);

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // ignore
            }
        }
    }

    public void start() {
        new Thread(this).start();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
