package edu.utcn.str.c2.threadrunnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName() + " will start the threads now");

        ThreadWithRunnableInterface r1 = new ThreadWithRunnableInterface("Ioan");
        ThreadWithRunnableInterface r2 = new ThreadWithRunnableInterface("Maria");
        ThreadWithRunnableInterface r3 = new ThreadWithRunnableInterface("Gheorghe");

        r1.start();
        r2.start();
        r3.start();
    }
}
